package com.glearning.dsa;

public class MergeSort {

	public static void main(String[] args) {
		int[] array = { 7, 3, 9, 4, 27, 6 };

		sort(array);

		// print the sorted array
		// the array should now be in sorted
		printArray(array);
	}

	private static void sort(int[] array) {
		// if there are no elements in the array, return
		if (array == null || array.length <= 1) {
			// System.out.println(array[0]);
			return;
		}

		// find the mid of the array
		int mid = array.length / 2;
		int[] left = new int[mid];
		int[] right = new int[array.length - mid];

		// copy all the elements of left array
		for (int i = 0; i < mid; i++) {
			left[i] = array[i];
		}

		// System.out.println("Mid is :: " + mid);
		// System.out.println("Element at mid is :: " + array[mid]);

		// copy all the elements of right array

		for (int i = mid; i < array.length; i++) {

			right[i - mid] = array[i];
		}

		// recursively sort the left array
		sort(left);
		// recursively sort the right array
		sort(right);

		// start with merging
		merge(left, right, array);
	}

	private static void merge(int[] left, int[] right, int[] array) {
		int i = 0;
		int j = 0;
		int k = 0;
		while (i < left.length && j < right.length) {
			// the element in the left sub-array is less than or equal to element in the
			// right array
			if (left[i] <= right[j]) {
				array[k] = left[i];
				i++;
				k++;
			}
			// the element in the left sub-array is less than or equal to element in the
			// right array
			else {
				array[k] = right[j];
				j++;
				k++;
			}
		}

		// copy the left over sorted elements from the left array
		while (i < left.length) {
			array[k] = left[i];
			i++;
			k++;
		}

		// copy the left over sorted elements from the right array
		while (j < right.length) {
			array[k] = right[j];
			j++;
			k++;
		}

	}

	private static void printArray(int[] array) {
		for (int element : array) {
			System.out.print(element + " ");
		}

	}

}
