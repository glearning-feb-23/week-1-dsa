package com.glearning.dsa;

public class BinarySearch {

	public static void main(String[] args) {
		int arr[] = {2,4,6,7,8,9,11};
	int value = binarySearch(arr, 0, arr.length, 13);
		System.out.println("The value found :: "+ value);
	}

	static int binarySearch(int arr[], int low, int high, int key) {

		int mid = (high + low) / 2;
		if (high < low || (mid > arr.length / 2)) {
			return -1;
		}


		System.out.println("mid value :: "+ mid);
		// happy path scenario where mid == key
		if (key == arr[mid]) {
			return mid;
		}
		// if the key is more than the mid
		if (key > arr[mid]) {
			return binarySearch(arr, (mid + 1), high, key);
		}
		return binarySearch(arr, low, (mid - 1), key);
	}

}
