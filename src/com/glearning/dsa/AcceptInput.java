package com.glearning.dsa;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AcceptInput {
	public static void main(String[] args) {
	
		System.out.println("Enter elements of an array in integer");
		System.out.println("Type q to quit");
		
		Scanner sc = new Scanner(System.in);
		boolean flag = true;
		List<Integer> list = new ArrayList<>();
		while(flag) {
			System.out.println("enter the next element or q to quit");
			String val = sc.next();
			if(val.equalsIgnoreCase("q")) {
				flag = false;
				continue;
			}
			list.add(Integer.parseInt(val));
		}
		
		Integer[] array = list.toArray(new Integer[0]);
		System.out.println(list);
	}

}
