package com.glearning.dsa;

public class ArrayLeftRotate {
	
	public static void main(String[] args) {
		
		int [] sortedArray= {2, 3, 4, 6, 9, 10, 56, 77};
		int mid = sortedArray.length / 2;
		int[] rotatedArray = leftRotate(sortedArray, mid);
		printArray(rotatedArray);
		
	}

	private static void printArray(int[] rotatedArray) {
		for(int val: rotatedArray) {
			System.out.print(val + " ");
		}
		
	}

	private static int[] leftRotate(int[] sortedArray, int mid) {
		//create a new array of length equal to the sorted array
		int[] rotatedArray = new int[sortedArray.length];
		int j = 0;
		//populate all the sorted elements after the mid position till the end of the array
		for(int i = mid; i < sortedArray.length; i ++) {
			rotatedArray[j] = sortedArray[i];
			j++;
		}
		
		//populate all the sorted elements from the beginning till the mid
		for(int i = 0; i< mid; i++) {
			rotatedArray[j] = sortedArray[i];
			j++;
		}
		
		return rotatedArray;
	}
	

}
